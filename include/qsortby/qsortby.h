#ifndef QSORTBY_H
#define QSORTBY_H

#include <stddef.h> // size_t

struct qsort_by_t {
    /**
     * One element of the input array
     */
    void *input;

    /**
     * One element that is ready for comparison
     */
    void *output;

    /**
     * This value stores the original index of the item
     *
     * This will be used for a tie breaker in the sorter
     */
    unsigned int index;
};

typedef int (*qsort_by_compare_func)(const void *, const void *);
typedef void* (*qsort_by_set_up_func)(const void *);
typedef void (*qsort_by_tear_down_func)(void *);

/**
 * The sorter function
 *
 * @param array     The array we want to sort
 * @param count     The number of items in the array
 * @param size      The size of each individual element in the array
 * @param comparer  A function which compares two values that were returned by the setup parameter
 * @param set_up    A function to which each element of the input will be fed to, and it's output will be compared
 *                  against each other, and the input array will be sorted by that
 * @param tear_down A function that frees up the memory. Pass NULL to not free up the memory.
 *                  You likely don't need to free up the memory if the setup function returns memory address
 *                  that was not allocated by the setup (pre-allocated memory)
 *                  as opposed to setup allocated memory.
 */
void qsort_by_free(void *array, size_t count, size_t size,
    qsort_by_compare_func comparer,
    qsort_by_set_up_func set_up,
    qsort_by_tear_down_func tear_down
);

/**
 * Same as `qsort_by_free()` function, but with a `NULL` passed in for `tear_down`
 */
void qsort_by(void *array, size_t count, size_t size,
    qsort_by_compare_func comparer,
    qsort_by_set_up_func set_up
);

#endif /* QSORTBY_H */
