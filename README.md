A C implementation of the Schwartzian transformation algorithm.

This function should be used when you are trying to sort an array with a comparison function that grew a bit expensive.
This sorting function runs a transformation function on each and every array item, then feeds the transformed data into the comparison function, which should be much much cheaper in turn.
Has two versions:
* The "free version" that frees up memory afterwards (transformation function allocates the memory it returns)
* The "non-free version" which doesn't free up the memory afterwards (transformation function returns a pointer to a memory address that was already allocated beforehand, like a property of a struct)

How to compile:
```bash
gcc -Wall -o Example example.c qsortby.c -I include/ -Og
```
