#include "qsortby/qsortby.h"
#include <stdlib.h> // Malloc
#include <string.h> // Memcpy

static qsort_by_compare_func compare_func = NULL;

int qsort_by_comparer(const void *a, const void *b) {
    // Convert comparer to a function returning int that expects two void pointers
    // Convert a and b into `qsort_by_t` structs and take their outputs
    int diff = compare_func(
        ((const struct qsort_by_t*)a)->output,
        ((const struct qsort_by_t*)b)->output
    );
    if (diff) {
        return diff;
    }

    // See: 1st shortcut on the qsort page:
    // https://en.cppreference.com/w/c/algorithm/qsort
    unsigned int ai = ((const struct qsort_by_t*)a)->index,
        bi = ((const struct qsort_by_t*)b)->index;
    return (ai < bi) - (ai > bi);
}

void qsort_by(void *array, size_t count, size_t size,
    qsort_by_compare_func comparer,
    qsort_by_set_up_func set_up
) {
    qsort_by_free(array, count, size, comparer, set_up, NULL);
}

/**
 * Allocates the qsort_by_t array and sets it up
 */
static struct qsort_by_t *qsort_by_set_up(void *array, size_t count, size_t size,
    qsort_by_set_up_func set_up
) {
    // First, allocate space for all the qsort_by_t structs
    struct qsort_by_t *elements = (struct qsort_by_t*)malloc(count * sizeof(struct qsort_by_t));

    // Run the setup on each array element
    for (unsigned int i = 0; i < count; i++) {
        elements[i].input = array + (i * size);
        elements[i].output = (set_up)(elements[i].input);
    }

    return elements;
}

/**
 * Deallocates the qsort_by_t array
 */
static void qsort_by_tear_down(struct qsort_by_t *elements,
    size_t count,
    qsort_by_tear_down_func tear_down
) {
    if (tear_down) {
        for (unsigned int i = 0; i < count; i++) {
            tear_down(elements[i].output);
        }
    }
    free(elements);
}

void qsort_by_free(void *array, size_t count, size_t size,
    qsort_by_compare_func comparer,
    qsort_by_set_up_func set_up,
    qsort_by_tear_down_func tear_down
) {
    // Then allocate enough space to store a new copy of the original array
    // TODO: Sort in-place instead
    void* array_copy = malloc(count * size);

    struct qsort_by_t *elements = qsort_by_set_up(array, count, size, set_up);


    // Sort shit
    // Store the comparer in a static variable, so we can sneak it into the compare function
    compare_func = comparer;
    qsort(elements, count, sizeof(struct qsort_by_t), qsort_by_comparer);
    compare_func = NULL;

    // Copy the original items to the copy array in the correct order
    for (unsigned int i = 0; i < count; i++) {
        memcpy(array_copy + (i * size), elements[i].input, size);
    }

    memcpy(array, array_copy, count * size);
    
    // Free the arrays
    free(array_copy);
    qsort_by_tear_down(elements, count, tear_down);
}
