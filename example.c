#include "qsortby/qsortby.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NAME_SIZE 20

struct Student
{
    char name[NAME_SIZE];
    int id;
    float score;
};

void *get_student_name(const void *student) {
    return ((struct Student*)student)->name;
}

void *get_student_score(const void *student) {
    return &(((struct Student*)student)->score);
}

void *get_student_id(const void *student) {
    return &(((struct Student*)student)->id);
}

void *get_student_name_length(const void *student) {
    size_t *length = (size_t*)malloc(sizeof(size_t));
    *length = strlen(((struct Student*)student)->name);
    return length;
}

int compare_student_names(const void *a, const void *b) {
    return strcmp((const char*)a, (const char*)b);
}

int compare_student_scores(const void *a, const void *b) {
    return *(int*)a - *(int*)b;
}

int compare_student_ids(const void *a, const void *b) {
    return *(int*)a - *(int*)b;
}

int compare_student_name_lengths(const void *a, const void *b) {
    return *(size_t*)a - *(size_t*)b;
}

void print_student(struct Student *students, int expectedIds[5], unsigned int index) {
    printf(
        "#%1d: %7s (%1d): %1.2f - expected: \e[%dm%7s\e[0m\n",
        index,
        students[index].name,
        students[index].id,
        students[index].score,
        students[index].id == expectedIds[index] ? 32 : 31,
        students[expectedIds[index]].name
    );
}

int main() {
    struct Student students[5];

    int id = 0;
    strncpy(students[id].name, "Mike", NAME_SIZE);
    students[id].id = id;
    students[id].score = 5.0;

    id = 1;
    strncpy(students[id].name, "Kate", NAME_SIZE);
    students[id].id = id;
    students[id].score = 3.0;

    id = 2;
    strncpy(students[id].name, "Marika", NAME_SIZE);
    students[id].id = id;
    students[id].score = 3.0;

    id = 3;
    strncpy(students[id].name, "Stephen", NAME_SIZE);
    students[id].id = id;
    students[id].score = 1.0;

    id = 4;
    strncpy(students[id].name, "Billy", NAME_SIZE);
    students[id].id = id;
    students[id].score = 4.0;

    printf("Before sorting:\n");
    for (size_t i = 0; i < 5; i++) {
        int expectedIds[5] = {0, 1, 2, 3, 4};
        print_student(students, expectedIds, i);
    }

    qsort_by(students, 5, sizeof(struct Student), compare_student_names, get_student_name);
    printf("\nAfter sorting by name:\n");
    for (size_t i = 0; i < 5; i++) {
        int expectedIds[5] = {4, 1, 2, 0, 3};
        print_student(students, expectedIds, i);
    }

    qsort_by(students, 5, sizeof(struct Student), compare_student_scores, get_student_score);
    printf("\nAfter sorting by score:\n");
    for (size_t i = 0; i < 5; i++) {
        int expectedIds[5] = {3, 1, 2, 4, 0};
        print_student(students, expectedIds, i);
    }
    
    qsort_by_free(students, 5, sizeof(struct Student), compare_student_name_lengths, get_student_name_length, free);
    printf("\nAfter sorting by name length:\n");
    for (size_t i = 0; i < 5; i++) {
        int expectedIds[5] = {1, 0, 4, 2, 3};
        print_student(students, expectedIds, i);
    }
    
    qsort_by(students, 5, sizeof(struct Student), compare_student_ids, get_student_id);
    printf("\nAfter sorting by id:\n");
    for (size_t i = 0; i < 5; i++) {
        int expectedIds[5] = {0, 1, 2, 3, 4};
        print_student(students, expectedIds, i);
    }
}
